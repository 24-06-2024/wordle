import requests as rq
import random
DEBUG = False

class MMBot:
    words = [word.strip() for word in open("words.txt")]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    creat_url = mm_url + "create"
    guess_url = mm_url + "guess"


    def __init__(self, name: str):
        def is_unique(w: str) -> bool:
            return len(w) == len(set(w))

        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(MMBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(MMBot.creat_url, json=creat_dict)

        self.choices = [w for w in MMBot.words[:] if is_unique(w)]
        random.shuffle(self.choices)
        word_length = 5
        self.correct = [None] * word_length
        self.present = [] 
        self.incorrect = set() 

    def play(self) -> str:
        def post(choice: str) -> tuple[int, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(MMBot.guess_url, json=guess)
            rj = response.json()
            right = int(rj["feedback"])
            status = "win" in rj["message"]
            return right, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        right, won = post(choice)
        tries = [f'{choice}:{right}']

        while not won:
            if DEBUG:
                print(choice, right, self.choices[:10])
            self.update(choice, right)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            right, won = post(choice)
            tries.append(f'{choice}:{right}')
        print("Secret is", choice, "found in", len(tries), "attempts")
        print("Route is:", " => ".join(tries))

    def update(self, choice: str, right: str):
        for i in range(len(choice)):
            if right[i] == 'G':
                self.correct[i] = choice[i]
            elif right[i] == 'Y':
                if choice[i] not in self.present:
                    self.present.append(choice[i])
            elif right[i] == 'B':
                self.incorrect.add(choice[i])
        self.choices = [w for w in self.choices if self.form_guess(choice, w)]
        

    def form_guess(self) -> str:
        for word in self.choices:
            match = True
            for i in range(len(word)):
                if self.correct[i] is not None and self.correct[i] != word[i]:
                    match = False
                    break
                return True





game = MMBot("CodeShifu")
game.play()

