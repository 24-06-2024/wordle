Team: 
Alpana Mohanty,
Hitha Ankam,
Pradeepaa M,
Kavya Rai

About the project:
This is a clone of the word guessing game- Wordle. Guess the 5-letter word in 6 tries. Letters turn Green, Yellow, or Red based on their position.

Wordle uses a simple color-coding system to provide clues after each guess:
Green: The letter is in the word and in the correct spot.
Yellow: The letter is in the word but in the wrong spot
Red: The letter is not in the word in any spot.
