import requests as rq
import random

DEBUG = True

class MMBot:
    words = [word.strip() for word in open("words.txt")]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    creat_url = mm_url + "create"
    guess_url = mm_url + "guess"

    def __init__(self, name: str):
        def is_unique(w: str) -> bool:
            return len(w) == len(set(w))

        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(MMBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(MMBot.creat_url, json=creat_dict)

        self.choices = [w for w in MMBot.words[:] if is_unique(w)]
        random.shuffle(self.choices)

    def play(self) -> str:
        def post(choice: str) -> tuple[int, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(MMBot.guess_url, json=guess)
            rj = response.json()
            right = rj["feedback"]
            status = "win" in rj["message"]
            return right, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        right, won = post(choice)
        print(post(choice))
        tries = [f'{choice}:{right}']

        for _ in range(5):
            if won:
                break
            if DEBUG:
                print(choice, right, self.choices[:10])
            self.update(choice, right)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            right, won = post(choice)
            tries.append(f'{choice}:{right}')
        
        if won:
            print("Secret is", choice, "found in", len(tries), "attempts")
        else:
            print("Failed to find the secret word in 6 attempts.")
        print("Route is:", " => ".join(tries))

    def update(self, choice: str, right: str):
        def letter_position_G(choice, right):
            print([(i,choice[i]) for i, v in enumerate(right) if v == "G" ])
            return [(i,choice[i]) for i, v in enumerate(right) if v == "G" ]
        def letter_position_Y(choice, right):
            print([choice[i] for i, v in enumerate(right) if v == "Y" ])
            return [choice[i] for i, v in enumerate(right) if v == "Y" ]
        def letter_position_R(choice, right):
            print([choice[i] for i, v in enumerate(right) if v == "R" ])
            return [choice[i] for i, v in enumerate(right) if v == "R" ]
            

        def common(choice: str, word: str):
            ler_pos_crct = letter_position_G(choice, right)
            ler_crct = letter_position_Y(choice, right)
            ler_wrng = letter_position_R(choice,right)
            if ler_pos_crct:
                for i in range(len(ler_pos_crct)):
                    if word[i][0] != ler_pos_crct[i][1]:
                        return False
            if ler_crct and len(set(ler_crct) & set(word)) == 0:
                return False
            '''if ler_wrng and len(set(ler_crct) & set(word)) != 0:
                return False'''
            return True          
        
        self.choices = [w for w in self.choices if common(choice, w)]


game = MMBot("CodeShifu")
game.play()


